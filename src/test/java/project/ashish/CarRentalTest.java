package project.ashish;
import static java.time.LocalDateTime.parse;
import static org.junit.Assert.assertTrue;
import static project.ashish.constants.RentalConstants.dateTimeFormatter;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import project.ashish.constants.CarType;
import project.ashish.constants.Message;
import project.ashish.constants.RentalWrapper;
import project.ashish.framework.CarRentalService;
import project.ashish.framework.MockCarRentalServiceImpl;
public class CarRentalTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@BeforeClass
	public static void setUpClass() {
		carRentalService = new MockCarRentalServiceImpl(); 
	    //executed only once, before the first test
	}
	
	private static CarRentalService carRentalService; 
	

	//Book a sedan with Nov3 date for 17 days for customer 4
	@Test
	public void testScneario1() throws Exception { 
		RentalWrapper bookingResults = carRentalService.rentCar(CarType.valueOf("SEDAN"),parse("11:03:2017:22:45",dateTimeFormatter), 17,4); 
		assertTrue(bookingResults!=null && bookingResults.getMessage()==Message.BOOKING_COMPLETE);
	}

	//Book multiple vans . In our test data, we have only 2 vans. So try booking 3 vans
	@Test
	public void testScenario2() throws Exception { 
		RentalWrapper bookingResults1 = carRentalService.rentCar(CarType.valueOf("VAN"),parse("11:03:2017:22:45",dateTimeFormatter), 17,3); 
		assertTrue(bookingResults1!=null && bookingResults1.getMessage()==Message.BOOKING_COMPLETE);
		RentalWrapper bookingResults2 = carRentalService.rentCar(CarType.valueOf("VAN"),parse("11:03:2017:22:45",dateTimeFormatter), 17,5); 
		assertTrue(bookingResults2!=null && bookingResults2.getMessage()==Message.BOOKING_COMPLETE);
		RentalWrapper bookingResults3 = carRentalService.rentCar(CarType.valueOf("VAN"),parse("11:03:2017:22:45",dateTimeFormatter), 17,4); 
		assertTrue(bookingResults3!=null && bookingResults3.getMessage()==Message.BOOKING_CAR_UNAVAILABLE);
	}


	//Book sedan for customer 1 who already has a future booking
	@Test
	public void testScenario3() throws Exception { 
		RentalWrapper bookingResults = carRentalService.rentCar(CarType.valueOf("SEDAN"),parse("11:03:2017:22:45",dateTimeFormatter), 17,1); 
		assertTrue(bookingResults!=null && bookingResults.getMessage()==Message.BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY);
	}

	

	@Test
	public void testForNoArg() throws Exception{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(Message.INVALID_CMD_ARGS.val());
		CarRentalCaller.main(new String[] {});
	}

	@Test
	public void testForOnlyOneArg() throws Exception{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(Message.INVALID_CMD_ARGS.val());
		CarRentalCaller.main(new String[] {"12"});
	}

	//Run some invalid test cases for arguments 


	@Test
	public void invalidDate() throws Exception{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(Message.INVALID_DATE_OR_TIME.val());
		CarRentalCaller.main(new String[] {"SEDAN", "11:03:2017:233:45","14","4"});
	}

	@Test
	public void invalidCustomer() throws Exception{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(Message.INVALID_CUSTOMER_ID.val());
		CarRentalCaller.main(new String[] {"SEDAN", "11:03:2017:22:45","14","absd"});
	}

	@Test
	public void invalidRentalDays() throws Exception{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(Message.INVALID_RENTAL_DAYS.val());
		CarRentalCaller.main(new String[] {"SEDAN", "11:03:2017:22:45","112","3"});
	}
	





}
