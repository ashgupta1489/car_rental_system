package project.ashish.constants;

public enum CarStatus {

	AVAILABLE,RESERVED,SERVICING,WITH_CUSTOMER;
}
