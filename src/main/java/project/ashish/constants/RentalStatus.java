package project.ashish.constants;

public enum RentalStatus {

	IN_PROGRESS,FINISHED,RESERVED,CANCELLED;
}
