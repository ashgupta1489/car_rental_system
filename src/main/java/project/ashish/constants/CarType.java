package project.ashish.constants;

public enum CarType {
	SEDAN,SUV,VAN;
	
	public static boolean contains(String carTypeString) {
	    for (CarType type : CarType.values()) {
	        if (type.name().equals(carTypeString)) {
	            return true;
	        }
	    }
	    return false;
	}


}
