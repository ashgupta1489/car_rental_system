package project.ashish.constants;

import java.util.Optional;

import project.ashish.model.Rental;

public class RentalWrapper {

	private Rental rental;
	private Message message;
	private Optional<Exception> exception;
	public Rental getRental() {
		return rental;
	}
	public void setRental(Rental rental) {
		this.rental = rental;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public Optional<Exception> getException() {
		return exception;
	}
	public void setException(Optional<Exception> exception) {
		this.exception = exception;
	}
	@Override
	public String toString() {
		return "RentalWrapper [" + (rental != null ? "rental=" + rental + ", " : "")
				+ (message != null ? "message=" + message.val() + ", " : "")
				+ (exception != null ? "exception=" + exception.toString() : "") + "]";
	}
	
	
}
