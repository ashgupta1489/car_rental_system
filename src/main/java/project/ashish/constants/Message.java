package project.ashish.constants;

/**
 * This class can be used for any type of message, it can be error, simple message. 
 * @author Ashish
 */
public enum Message{
	INVALID_CMD_ARGS(1,"Arguments Missing or more. Four Arguments needed in order. Car Type (SEDAN,SU,VAN), Desired Datetime(mm:dd:yyyy:HH:mm), Desired Days(1 to 90), Customer_Id (integer)"),
	INVALID_RENTAL_DAYS(2,"Invalid Rental Days. Keep it between 1 to 90"),
	INVALID_DATE_OR_TIME(3,"Invalid Date-Time format. It should be in mm:dd:yyyy:HH:mm format and atleast 60 mins after current time"),
	INVALID_CAR_TYPE(4,"Invalid Car Type provide. Select either of SEDAN, SUV, VAN"),
	INVALID_CUSTOMER_ID(5,"Invalid Customer Id. Either not a valid customer id (integer)"),


	NO_PRIOR_BOOKING(8,"No valid prior booking. No car for pickup."),
	NO_CAR_RENTAL_IN_PROGRESS(9,"Car rental not in progress for return"),

	BOOKING_CAR_UNAVAILABLE(6,"Car not Available with particular type for renting"),
	BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY(7,"Customer already has a car rental in progress or booking in future"),
	BOOKING_DUP_RESERVATION(13,"Rental Exist for Customer on Same Date") ,
	BOOKING_UNABLE(14,"Unable to book the renal for some unknown results"),
	UNABLE_TO_CREATE_OBJ(15,"Unable to create object"),
	BOOKING_COMPLETE(16,"Booked");

	private final int code;
	private final String desc;

	private Message(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String val() {
		return desc;
	}

	@Override
	public String toString() {
		return "Code:"+ code + ", Desc: " + desc;
	}
}