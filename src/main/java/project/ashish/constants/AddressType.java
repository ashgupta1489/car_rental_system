package project.ashish.constants;

public enum AddressType {

	MAILING_ADDRESS,BILLING_ADDRESS,RENTAL_LOCATION,RETURN_LOCATION;
}
