package project.ashish.constants;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;
/**
 * Constant class
 * To make it easy to change, we can also change it to property file and read it  
 * @author Ashish
 *
 */
public interface RentalConstants {

	static final int MAX_ARGS=4;
	static final int MIN_RENTAL_DAYS_ALLOWED=1;
	static final int MAX_RENTAL_DAYS_ALLOWED=90;
	static final String COMMA = ",";
	static final String SPACE = " ";
	static final String NEW_LINE=System.getProperty("line.separator");
	static final int MINIMUM_BOOKING_WAIT_TIME = 60;
	static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM:dd:yyyy:HH:mm");
	static final AtomicInteger carSeq = new AtomicInteger(1000);
	static final AtomicInteger rentalSeq = new AtomicInteger(100);
	static final AtomicInteger custSeq = new AtomicInteger();
}