package project.ashish.framework;

import java.util.Collection;

public class RentalUtilities {

	public static boolean isEmpty(Collection<?> col) {
		return col == null || col.isEmpty();
	}
	public static boolean isEmpty(String str) {
		return str == null || str.isEmpty();
	}
	
}
