package project.ashish.framework;

import project.ashish.constants.Message;

public class RentalException extends Exception{

	private static final long serialVersionUID = 1L;

    public RentalException(Message message) {
        super(message.toString());
    }

    public RentalException(Message message, Throwable cause) {
        super(message.toString(), cause);
    }

}