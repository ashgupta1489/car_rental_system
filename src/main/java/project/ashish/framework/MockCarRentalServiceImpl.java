package project.ashish.framework;
import static project.ashish.constants.CarType.SEDAN;
import static project.ashish.constants.CarType.SUV;
import static project.ashish.constants.CarType.VAN;
import static project.ashish.constants.Message.INVALID_CUSTOMER_ID;
import static project.ashish.framework.RentalUtilities.isEmpty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import project.ashish.constants.CarType;
import project.ashish.constants.Message;
import project.ashish.constants.RentalStatus;
import project.ashish.constants.RentalWrapper;
import project.ashish.model.Car;
import project.ashish.model.Customer;
import project.ashish.model.Rental;



/**
 *  This simple file service generate mock data . Similar can be written for database or file based.
 * @author Ashish
 *
 */
public class MockCarRentalServiceImpl implements CarRentalService {

	private final  List<Car> cars = new ArrayList<>();
	private  final  List<Customer> customers = new ArrayList<>();
	private  List<Rental> rentals = new ArrayList<>();

	public MockCarRentalServiceImpl() {
		this.createData();
	}


	@Override
	public Customer getCustomer(int customerId) {
		return customers.stream().filter(c -> c.getCustomerId()==customerId).findFirst().orElse(null);
	}


	/**
	 * Temporary Data . should be coming from backend services
	 * @throws Exception
	 */
	private void createData(){
		try {
			if(isEmpty(rentals) || isEmpty(customers) || isEmpty(cars)) {
				//Creating local-time 
				LocalDateTime datetime1 = LocalDateTime.of(2016, 11, 2, 9, 30);
				LocalDateTime datetime2 = LocalDateTime.of(2017, 11, 27, 9, 30);
				LocalDateTime datetime3 = LocalDateTime.of(2017, 11, 2, 9, 30);
				LocalDateTime datetime4 = LocalDateTime.of(2017, 12, 20, 21, 30);
				LocalDateTime datetime5 = LocalDateTime.of(2017, 5, 11, 9, 45);
				LocalDateTime datetime6 = LocalDateTime.of(2017, 10, 29, 2, 30);

				//Creating cars
				Car car1 = new Car(SEDAN, "PLK912","GREY","Subaru Impreza");
				Car car2 = new Car(SEDAN, "PLK913","RED","Audi TT4");
				Car car3 = new Car(SEDAN, "PLK914","YELLOW","Mercedes C300");
				Car car4 = new Car(SUV, "PLK915","BLACK","Toyota Abcd");
				Car car5 = new Car(SEDAN, "PLK916","WHITE","Lexus LCF30");
				Car car6 = new Car(SUV, "PLK917","GREY","BMW X5");
				Car car7 = new Car(SEDAN, "PLK918","ORANGE","Honda Civic");
				Car car8 = new Car(SUV, "PLK919","GREEN","Nissan Rogue");
				Car car9 = new Car(SEDAN, "PLK920","SILVER","Hyundai Genesis");
				Car car10 = new Car(VAN, "PLK921","BLUE","Ford F50");
				Car car11= new Car(VAN, "PLK922","PURPLE","Chevrolet RAM");
				Car car12 = new Car(SUV, "PLK923","BROWN","Toyota RAV4");

				// Creating customers
				Customer customer1 = new Customer.Builder().firstName("Ashish").lastName("Gupta").emailAddress("as@vx.com")
						.homePhnNum("333-122-2323").address("1100 Ashborn Road, Charleston").build();

				Customer customer2 = new Customer.Builder().firstName("James").lastName("Smith").address("1200 Ashborn Road, Charleston").build();
				Customer customer3 = new Customer.Builder().firstName("John").lastName("Doe").address("1300 Ashborn Road, Charleston").build();
				Customer customer4 = new Customer.Builder().firstName("Betty").lastName("Whihte").address("1400 Ashborn Road, Charleston").build();
				Customer customer5 = new Customer.Builder().firstName("Jason").lastName("Bourn").address("1500 Ashborn Road, Charleston").build();



				Rental rental1 = new Rental(datetime1, 10, car1, customer1);
				rental1.setActualReturnDate(LocalDateTime.of(2016, 11, 12, 9, 30));
				rental1.updateStatus(RentalStatus.FINISHED);
				Rental rental2 = new Rental(datetime2, 20, car2, customer1);
				Rental rental3 = new Rental(datetime3, 5, car12, customer2);
				Rental rental4 = new Rental(datetime4, 6, car6, customer3);
				Rental rental5 = new Rental(datetime5, 7, car10, customer4);
				rental5.setActualReturnDate(LocalDateTime.of(2017, 5, 18, 9, 45));
				rental1.updateStatus(RentalStatus.FINISHED);
				Rental rental6 = new Rental(datetime6, 1, car2, customer4);


				//Setting up relationships
				customer1.addRental(rental1);
				customer1.addRental(rental2);
				customer2.addRental(rental3);
				customer2.addRental(rental4);
				customer2.addRental(rental5);
				customer2.addRental(rental6);

				cars.add(car1);cars.add(car2);cars.add(car3);cars.add(car4);
				cars.add(car5);cars.add(car6);cars.add(car7);cars.add(car8);
				cars.add(car9);cars.add(car10);cars.add(car11);cars.add(car12);

				rentals.add(rental1);rentals.add(rental2);
				rentals.add(rental3);rentals.add(rental4);
				rentals.add(rental5);rentals.add(rental6);
				customers.add(customer1);customers.add(customer2);
				customers.add(customer3);customers.add(customer4);customers.add(customer5);

				//Printing the data 
				/*cars.stream().forEach(car -> System.out.println(car));
			customers.stream().forEach(cust -> System.out.println(cust));
			rentals.stream().forEach(rental -> System.out.println(rental));

			for (CarType typeVar: CarType.values()) {
				List<Car> carsByType = cars.stream().filter(c -> c.getType()==typeVar).collect(Collectors.toList());
				System.out.println("Cars with type :"+ typeVar+ " Count = "+ carsByType.size());
				carsByType.stream().forEach(car -> System.out.println(car));
			}*/

			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public RentalWrapper rentCar(CarType desiredType, LocalDateTime desiredDateTime, Integer desiredDays, int customerId) {
		RentalWrapper wrapper = new RentalWrapper();
		try {
			//this.createData(); // In real world would be removed
			System.out.println("Car Type selected is: "+  desiredType.name());
			System.out.println("Desired Date-time is: "+  desiredDateTime.toString());
			System.out.println("Desired Days are: "+  desiredDays);
			System.out.println("Requesting Customer Id: "+ customerId);

			//Get customer
			Customer customer =  this.getCustomer(customerId);
			if(customer==null) {
				throw new IllegalArgumentException(INVALID_CUSTOMER_ID.val());
			}

			List<Car> possibleCarsAvailable= cars.stream().filter(c -> c.getType()==desiredType && c.isAvailableOn(desiredDateTime, desiredDays))
					.collect(Collectors.toList());
			if(isEmpty(possibleCarsAvailable)) {
				wrapper.setMessage(Message.BOOKING_CAR_UNAVAILABLE);
				return wrapper;
			}
			Rental rental = customer.rent(possibleCarsAvailable.get(0), desiredDateTime, desiredDays);
			rentals.add(rental);
			wrapper.setRental(rental);
			wrapper.setMessage(Message.BOOKING_COMPLETE);
			System.out.println(rental.toString());
		}
		catch(RentalException rex) {
			if(rex.getMessage().equalsIgnoreCase(Message.BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY.toString())) {
				wrapper.setMessage(Message.BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY);
				wrapper.setException(Optional.of(rex));
			}
		}
		catch(Exception ex) {
			ex.printStackTrace();
			wrapper.setMessage(Message.BOOKING_UNABLE);
			wrapper.setException(Optional.of(ex));
		}
		return wrapper;
	}



}