package project.ashish.framework;

import java.time.LocalDateTime;

import project.ashish.constants.CarType;
import project.ashish.constants.RentalWrapper;
import project.ashish.model.Customer;

/**
 * Interface which implements Data reading and basic functions for Car Rental Service 
 * 
 * @author Ashish
 *
 */
public interface CarRentalService {

	RentalWrapper rentCar(CarType desiredCarType, LocalDateTime desiredDateTime, Integer desiredDays, int customerId) throws Exception;
	Customer getCustomer(int customerId);
	
	default void printRentalInfo(RentalWrapper rentalWrapper) {
		System.out.println(rentalWrapper.toString());
		
	}
	
	
}
