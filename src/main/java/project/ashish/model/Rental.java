package project.ashish.model;

import static project.ashish.constants.RentalConstants.rentalSeq;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import project.ashish.constants.CarStatus;
import project.ashish.constants.Message;
import project.ashish.constants.RentalStatus;
import project.ashish.framework.RentalException;

/**
 *  We can capture billing and other details here
 */
public class Rental {
	private int rentalId;
	private Car car;
	private Customer customer;
	private LocalDateTime startDate;
	private LocalDateTime rentalEndDate; // actual return date
	private LocalDateTime actualReturnDate;
	private LocalDateTime bookingDate;
	private RentalStatus status;
	private String uniqueReservationId;
	private double price;

	
	public Rental(LocalDateTime rentalStartDate, int rentalDuration, Car car, Customer customer) throws RentalException,IllegalArgumentException {
		if(car==null || customer==null) {
			throw new RentalException(Message.UNABLE_TO_CREATE_OBJ);
		}
		this.rentalId = rentalSeq.getAndIncrement();
		this.startDate = rentalStartDate;
		this.rentalEndDate = rentalStartDate.plusDays(rentalDuration);
		this.customer = customer;
		this.bookingDate= LocalDateTime.now();
		this.uniqueReservationId= UUID.randomUUID().toString().replace("-", ""); //Its not efficient 
		car.updateStatus(CarStatus.RESERVED);
		car.addRental(this);
		this.car=car;
		this.status= RentalStatus.RESERVED;
	}

	public int getRentalId() {
		return rentalId;
	}

	public Car getCar() {
		return car;
	}

	public Customer getCustomer() {
		return customer;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getRentalEndDate() {
		return rentalEndDate;
	}


	public LocalDateTime getActualReturnDate() {
		return actualReturnDate;
	}


	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public RentalStatus getStatus() {
		return status;
	}

	public String getUniqueReservationId() {
		return uniqueReservationId;
	}

	public void updateStatus(RentalStatus status) {
		this.status = status;
	}

	public void setActualReturnDate(LocalDateTime actualReturnDate) {
		this.actualReturnDate = actualReturnDate;
	}


	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}




	public int getActualRentedDays() {
		Long days= ChronoUnit.DAYS.between(actualReturnDate, startDate);
		return days.intValue();
	}

	public int getBookingDays() {
		Long days= ChronoUnit.DAYS.between(rentalEndDate, startDate);
		return days.intValue();
	}

	public void finishRental() throws RentalException {
		if (!isInProgress()) {
			throw new RentalException(Message.NO_CAR_RENTAL_IN_PROGRESS);
		}
		customer.finishRental();
	}


	public boolean isInProgress() {
		return status==RentalStatus.IN_PROGRESS;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	//*************** GENERIC METHOD *****************
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rentalId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rental other = (Rental) obj;
		if (rentalId != other.rentalId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Rental [rentalId=" + rentalId + ", " + (car != null ? "car_id=" + car.getCarId() + ", " : "")
				+ (customer != null ? "customer_id=" + customer.getCustomerId() + ", " : "")
				+ (startDate != null ? "startDate=" + startDate + ", " : "")
				+ (rentalEndDate != null ? "rentalEndDate=" + rentalEndDate + ", " : "")
				+ (actualReturnDate != null ? "actualReturnDate=" + actualReturnDate + ", " : "")
				+ (bookingDate != null ? "bookingDate=" + bookingDate + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (uniqueReservationId != null ? "uniqueReservationId=" + uniqueReservationId : "") + "]";
	}





}