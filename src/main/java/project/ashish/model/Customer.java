package project.ashish.model;

import static project.ashish.constants.RentalConstants.custSeq;
import static project.ashish.framework.RentalUtilities.isEmpty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import project.ashish.constants.CarStatus;
import project.ashish.constants.Message;
import project.ashish.constants.RentalStatus;
import project.ashish.framework.RentalException;
/** 
 * This class extends Base User class and hold details specific to customer
 * User class hold details like username/email/ last_accessed date and logged in status
 *  *  The user class would be a a superclass of Employees/Admins and Customers 
 *  The contact details can be moved to another class called contact
 *  This class is build upon Builder pattern
 */
public class Customer extends User{
	private int customerId; // in real world, it would be long
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String homePhnNum;
	private String address;
	private List<Rental> rentals;


	public Customer(int customerId) {
		this.customerId = customerId;
		//Check if customer Id exist and return the object from DB
	}


	private Customer(Builder builder) {
		this.customerId = builder.customerId;
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.emailAddress = builder.emailAddress;
		this.homePhnNum = builder.homePhnNum;
		this.address= builder.address;
	}

	/** ACTIONS **/

	public void addRental(Rental rental) {
		if(rentals==null) {
			rentals = new ArrayList<>();
		}
		rentals.add(rental); // can do validation to check if duplicate exists. can synchronize method
	}

	public List<Rental> getRentals() {
		return  new ArrayList<>(rentals);
	}

	public Rental getCurrentRental() {
		if(!isEmpty(rentals)) {
			return rentals.stream().filter(rental -> rental.getStatus().equals(RentalStatus.IN_PROGRESS)).findFirst().orElse(null);
		}
		return null;
	}

	// Will return future bookings, past and cancelled 
	public List<Rental> getRentalsByType(RentalStatus rentalStatus) {
		List<Rental> rentalsByType = new ArrayList<>();
		if(!isEmpty(rentals)) {
			rentalsByType = rentals.stream().filter(rental -> rental.getStatus().equals(rentalStatus)).collect(Collectors.toList());
		}
		return rentalsByType;
	}

	//Allow only if not having current rental in progress  or doesn't have future reservations and car is available
	public Rental rent(Car car,LocalDateTime rentalStartDate, int rentalDuration) throws RentalException {

		if (getCurrentRental() != null  || !isEmpty(getRentalsByType(RentalStatus.RESERVED))) {
			throw new RentalException(Message.BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY);
		}
		Rental rental = new Rental(rentalStartDate,rentalDuration,car,this);// first reserve 
		addRental(rental);
		return rental;
	}


	//Move the status to inprogress
	public Rental pickCar() throws RentalException {
		if (getCurrentRental() != null) {
			throw new RentalException(Message.BOOKING_CUST_IN_PROGRESS_RENTAL_ALREADY);
		}

		Rental rental = getRentalsByType(RentalStatus.RESERVED).stream().min(
				Comparator.comparing(Rental::getStartDate, Comparator.nullsLast(Comparator.reverseOrder()))).orElse(null);

		if(!isAvailableForPickup(rental)) {
			throw new RentalException(Message.NO_PRIOR_BOOKING);
		}
		rental.updateStatus(RentalStatus.IN_PROGRESS);
		rental.getCar().updateStatus(CarStatus.WITH_CUSTOMER);
		rental.getCar().updateRental(rental);
		return rental;
	}

	// rental  null || rental start today and after booking time 
	private boolean isAvailableForPickup(Rental rental) {
		LocalDateTime now = LocalDateTime.now();
		if(rental!=null && rental.getStartDate().isBefore(now) && rental.getStartDate().toLocalDate().equals(now.toLocalDate())) {
			return true;
		}
		return false;
	}

	//Give the car back to pool and mark rental as finished
	public Rental finishRental() throws RentalException {
		Rental rental = getCurrentRental();
		if (rental == null) {
			throw new RentalException(Message.NO_CAR_RENTAL_IN_PROGRESS);
		}
		rental.getCar().updateStatus(CarStatus.AVAILABLE);
		rental.getCar().updateRental(rental);
		rental.updateStatus(RentalStatus.FINISHED);
		rental.setActualReturnDate(LocalDateTime.now());
		return rental;
	}

	public Rental cancelReservation() throws RentalException{
		Rental rental = getCurrentRental();
		if (rental == null) {
			throw new RentalException(Message.NO_CAR_RENTAL_IN_PROGRESS);
		}
		rental.getCar().updateStatus(CarStatus.AVAILABLE);
		rental.getCar().updateRental(rental);
		rental.updateStatus(RentalStatus.CANCELLED);
		return rental;
	}

	//Validation can be put in here 
	public static class Builder {
		private String firstName;
		private String lastName;
		private String emailAddress;
		private String homePhnNum;
		private String address;
		private final int customerId;
		public Builder() {
			this.customerId = custSeq.incrementAndGet();
		}

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder emailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
			return this;
		}

		public Builder homePhnNum(String homePhnNum) {
			this.homePhnNum = homePhnNum;
			return this;
		}

		public Builder address(String address) {
			this.address = address;
			return this;
		}



		public Customer build() {
			return new Customer(this);
		}

	}


	public int getCustomerId() {
		return customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getHomePhnNum() {
		return homePhnNum;
	}
	public String getAddress() {
		return address;
	}

	//*************** GENERIC METHOD *****************


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + customerId;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customerId != other.customerId)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", " + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (emailAddress != null ? "emailAddress=" + emailAddress + ", " : "")
				+ (homePhnNum != null ? "homePhnNum=" + homePhnNum + ", " : "")
				+ (address != null ? "address=" + address : "") + "]";
	}



}