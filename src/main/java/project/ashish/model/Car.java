package project.ashish.model;

import static project.ashish.constants.CarStatus.AVAILABLE;
import static project.ashish.constants.CarStatus.WITH_CUSTOMER;
import static project.ashish.constants.RentalConstants.carSeq;
import static project.ashish.framework.RentalUtilities.isEmpty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import project.ashish.constants.CarStatus;
import project.ashish.constants.CarType;
import project.ashish.constants.Message;
import project.ashish.constants.RentalStatus;
import project.ashish.framework.RentalException;
/*
		We can create a manufacturer class to show manufacturar related 
		details with  Name (like Subaru, Toyota,..) , 
		last recall dates and like that 
		Similarly. we can create records of Services done on this car.. a
 */
//We can extend it to Vehicle if we want to include other types as well . 
public class Car {

	//﻿Car_Id,Type,PlateNum,Color,
	private int carId;
	private CarType type;
	private CarStatus status; 
	private String carModel;
	private String licensePlate;
	private String color;
	private double rate;
	private List<Rental> rentals; // one car can have multiple reservation

	//private CarDetail caDetail;
	// car Model, Vin number, manufacturer 

	public Car(CarType carType, String licensePlate, String color, String carModel) throws RentalException{
		if(carType==null || isEmpty(licensePlate)|| isEmpty(color) || isEmpty(carModel)) {
			throw new RentalException(Message.UNABLE_TO_CREATE_OBJ);
		}
		this.carId = carSeq.incrementAndGet();
		this.type= carType;
		this.color = color;
		this.carModel = carModel;
		this.licensePlate = licensePlate;
		this.status = AVAILABLE; // initially mark it as available
		//Set rates . 
	}

	public String getCarModel() {
		return carModel;
	}

	public String getColor() {
		return color;
	}

	public int getCarId() {
		return carId;
	}
	public CarType getType() {
		return type;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public CarStatus getStatus() {
		return status;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}

	public void updateStatus(CarStatus status) {
		this.status = status;
	}

	public List<Rental> getRentals() {
		return new ArrayList<>(rentals);
	}
	public void addRental(Rental rental) {
		if(rentals==null) {
			rentals = new ArrayList<>();
		}
		rentals.add(rental); // can do validation to check if duplicate exists. can synchronize method
	}

	public boolean isCurrentlyAvailable() {
		return status!=null && status==AVAILABLE;
	}

	public boolean isAvailableOn(LocalDateTime desiredDate, int localDuration) {
		if(!isEmpty(rentals)) {
			for (Rental rental : rentals) {
				if(rental.isInProgress() && rental.getRentalEndDate().isAfter(desiredDate)) {
					return false;
				}
				if(rental.getStatus()==RentalStatus.RESERVED && 
						(rental.getRentalEndDate().isAfter(desiredDate))){
					return false;
				}
			}
			return true;
		}
		return true;
	}

	public boolean isWithCustomer() {
		return status!=null && status==WITH_CUSTOMER;
	}


	//*************** GENERIC METHOD *****************
	@Override
	public String toString() {
		return "Car [carId=" + carId + ", " + (type != null ? "type=" + type + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (carModel != null ? "carModel=" + carModel + ", " : "")
				+ (licensePlate != null ? "licensePlate=" + licensePlate + ", " : "")
				+ (color != null ? "color=" + color : "") + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + carId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (carId != other.carId)
			return false;
		return true;
	}

	public void updateRental(Rental rental) {
		if(!isEmpty(rentals)){
			for (Iterator<Rental> iterator = rentals.iterator(); iterator.hasNext();) {
				Rental r = (Rental) iterator.next();
				if(r.getRentalId()==rental.getRentalId()) {
					iterator.remove();
				}
			}
			rentals.add(rental);
		}

	}




}