package project.ashish;
import static project.ashish.constants.Message.INVALID_CAR_TYPE;
import static project.ashish.constants.Message.INVALID_CMD_ARGS;
import static project.ashish.constants.Message.INVALID_CUSTOMER_ID;
import static project.ashish.constants.Message.INVALID_DATE_OR_TIME;
import static project.ashish.constants.Message.INVALID_RENTAL_DAYS;
import static project.ashish.constants.RentalConstants.MAX_ARGS;
import static project.ashish.constants.RentalConstants.MAX_RENTAL_DAYS_ALLOWED;
import static project.ashish.constants.RentalConstants.MINIMUM_BOOKING_WAIT_TIME;
import static project.ashish.constants.RentalConstants.MIN_RENTAL_DAYS_ALLOWED;
import static project.ashish.constants.RentalConstants.dateTimeFormatter;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import project.ashish.constants.CarType;
import project.ashish.constants.RentalWrapper;
import project.ashish.framework.CarRentalService;
import project.ashish.framework.MockCarRentalServiceImpl;
public class CarRentalCaller {

	/** Main entry method 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		System.out.println("Inside the Car Rental System"); 
		LocalDateTime desiredDateTime;
		Integer desiredDays;
		CarType desiredCarType;

		//Validating size of Arguments
		if(args.length!=MAX_ARGS ) {
			throw new IllegalArgumentException(INVALID_CMD_ARGS.val());
		}

		//Validating Car Type
		if(!CarType.contains(args[0])) {
			throw new IllegalArgumentException(INVALID_CAR_TYPE.val());
		}
		desiredCarType = CarType.valueOf(args[0]);

		//Validating rental required time
		try{
			desiredDateTime =  LocalDateTime.parse(args[1],dateTimeFormatter);
			if(!desiredDateTime.isAfter(LocalDateTime.now().plusMinutes(MINIMUM_BOOKING_WAIT_TIME))) {
				throw new IllegalArgumentException(INVALID_DATE_OR_TIME.val());
			}
		}
		catch(DateTimeParseException dtpe) {
			throw new IllegalArgumentException(INVALID_DATE_OR_TIME.val());
		}

		//Validating Requested Rental Days
		try{
			desiredDays = Integer.valueOf(args[2]);
			if(desiredDays<MIN_RENTAL_DAYS_ALLOWED || desiredDays>MAX_RENTAL_DAYS_ALLOWED) {
				throw new IllegalArgumentException(INVALID_RENTAL_DAYS.val());
			}
		}
		catch(NumberFormatException nfe) {
			throw new IllegalArgumentException(INVALID_RENTAL_DAYS.val());
		}

		int customerId;
		try{
			customerId = Integer.valueOf(args[3]);
			if(customerId<1) {
				throw new IllegalArgumentException(INVALID_CUSTOMER_ID.val());
			}
		}
		catch(NumberFormatException nfe) {
			throw new IllegalArgumentException(INVALID_CUSTOMER_ID.val());
		}

		CarRentalService carRentalService = new MockCarRentalServiceImpl(); // Can be implemented with the help of dependency injection 
		RentalWrapper bookingResults = carRentalService.rentCar(desiredCarType, desiredDateTime, desiredDays,customerId); // Renting the car
		carRentalService.printRentalInfo(bookingResults);
		System.out.println("End of the Rental System");
	}

}