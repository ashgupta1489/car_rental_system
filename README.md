Car Rental System
-------------------

Submitted by Ashish Gupta 
	
Problem Statement:
-------------------	
Design and implement a car rental system using object-oriented principles. 

1. The system should allow reserving a car of a given type at a desired date and time for a given number of days.
2. There are 3 types of cars (sedan, SUV and van).
3. The number of cars of each type is limited.
4. Use Java as the implementation language. Prove that the system satisfies the requirements.
5. Recommended time for this task: 2-4 hours
	 
Assumption:
-------------------	
Following assumptions are taken 

1. No web application, database and holding session of customer
2. Customer ask for a car type, system automatically provides a car from available data. 
3. Mock Data is used to load customer,car and rental


Input:
-------------------	

1. Car Type (Sedan, Suv, Van in Capslock) SEDAN, SUV, VAN
2. Desired DateTime of Rental (mm:dd:yyyy:HH:mm format) - 11:03:2017:22:00
3. Desired days of Rental (Mimimum 1 to 90)  - 10
4. Unique Customer Id (Minimum - 1 , Maximum - Integer.MAX_VALUE) (long in real world)
	 
Output:
-------------------	
Output would be a printed on console with 

1. Booking Status
2. Any exceptions that might have occured with message code. 
3. Car Booking Details like  Desired Date, Desired Time, Booked On Time, Reservation Num , Car Number and other details


To build the application:
-------------------	
Required 
	Maven 
	Java 8 (Uses Java -8 features)
	
From the command line with Maven installed:

	$ mvn clean package

Run the application  
-------------------
Run java with providing input file in the command line.
 
	java -jar /path/to/car_rental-1.0.jar car_type date_time days cust_id	
	Ex: java -jar car_rental-1.0.jar SEDAN 11:03:2017:22:00 14 4